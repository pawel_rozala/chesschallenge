package net.rozalap.chesschallenge;

import net.rozalap.chesschallenge.core.ChallengeConfiguration;
import net.rozalap.chesschallenge.core.ChessBoard;

import java.util.List;

/**
 * Interface for Chess Challenge resolver class
 *
 * Created by prozala on 2015-06-28.
 */
public interface ChallengeResolver {

    /**
     * Provide the resolver with challenge configuration
     *
     * @param challengeConfiguration
     */
    void setChallengeConfiguration(ChallengeConfiguration challengeConfiguration);

    /**
     * Resolve the challenge
     */
    void resolveChallenge();

    /**
     * Generate a "pretty" string with solutions (valid chess boards)
     *
     * @return string with solutions
     */
    String prettyPrintValidBoards();

    /**
     * Provide a list of solutions found (for further processing/presentation)
     *
     * @return a list of valid challenge solutions
     */
    List<ChessBoard> getValidChessBoardsList();
}
