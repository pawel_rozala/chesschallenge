package net.rozalap.chesschallenge;

import net.rozalap.chesschallenge.core.ChallengeConfiguration;
import net.rozalap.chesschallenge.core.ChallengeResolverImpl;

import java.util.Scanner;

/**
 * Main class for ChessChallenge running ChallengeResolver for given ChallengeConfiguration
 * <p/>
 * Created by prozala on 2015-06-22.
 */
public class ChessChallenge {

    public static void main(String[] args) {

        try {

            Scanner in = new Scanner(System.in);

            System.out.println("Number of columns (M)?");
            int maxColumns = Integer.parseInt(in.nextLine());
            System.out.println("Number of rows (N)?");
            int maxRows = Integer.parseInt(in.nextLine());

            System.out.println("Number of kings?");
            int kingsNumber = Integer.parseInt(in.nextLine());
            System.out.println("Number of queens?");
            int queensNumber = Integer.parseInt(in.nextLine());
            System.out.println("Number of bishops?");
            int bishopsNumber = Integer.parseInt(in.nextLine());
            System.out.println("Number of rooks?");
            int rooksNumber = Integer.parseInt(in.nextLine());
            System.out.println("Number of knights");
            int knightsNumber = Integer.parseInt(in.nextLine());

            ChallengeConfiguration configuration = new ChallengeConfiguration(maxColumns, maxRows, kingsNumber, queensNumber, bishopsNumber, rooksNumber, knightsNumber);

            System.out.println(configuration.toString());

            ChallengeResolver resolver = new ChallengeResolverImpl();

            resolver.setChallengeConfiguration(configuration);

            resolver.resolveChallenge();

            System.out.println("Chess Challenge resolved. Unique solutions found: " + resolver.getValidChessBoardsList().size());

            System.out.println("Print the solutions? (please enter y for yes)");
            String print = in.nextLine();
            if ("y".equals(print)) {
                System.out.println(resolver.prettyPrintValidBoards());
            }
        } catch (NumberFormatException e) {
            System.out.println("Input should be a number. Please run the program again.");
            e.printStackTrace();
        }

    }

}
