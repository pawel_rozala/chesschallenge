package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.ChallengeResolver;
import net.rozalap.chesschallenge.core.exception.InvalidConfigurationException;
import net.rozalap.chesschallenge.core.exception.NoConfigurationException;
import net.rozalap.chesschallenge.pieces.ChessPiece;
import net.rozalap.chesschallenge.utils.ChallengeResolverUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Class resolves the Chess Challenge problem by evaluating all possible piece placements (beginning with starting square 0,0)
 * for valid boards (all pieces placed without conflict) and all possible piece sequences (order of placement).
 * <p/>
 * Created by prozala on 2015-06-23.
 */
public class ChallengeResolverImpl implements ChallengeResolver {

    final static Logger logger = Logger.getLogger(ChallengeResolver.class);

    private ChallengeConfiguration configuration;

    private Map<String, ChessBoard> validChessBoards = new HashMap<String, ChessBoard>();

    public ChallengeResolverImpl() {
    }

    public void setChallengeConfiguration(ChallengeConfiguration challengeConfiguration) {
        validateChessBoardDimensions(challengeConfiguration);
        this.configuration = challengeConfiguration;
        logger.debug(configuration.toString());
    }

    public void resolveChallenge() {
        if (configuration != null) {
            logger.info("Start resolving the Chess Challenge");

            //create all permutations of sequence of pieces
            String pieceSequence = configuration.getPieceSequence();
            logger.debug("Original piece sequence: " + pieceSequence);

            Set<String> permutationsOfPieceSequence = ChallengeResolverUtils.generatePieceSequencePermutations(pieceSequence);
            if (logger.isDebugEnabled()) {
                logger.debug("Generated piece sequences");
                for (String sequence : permutationsOfPieceSequence) {
                    logger.debug(sequence);
                }
            }

            for (String sequence : permutationsOfPieceSequence) {
                //try to resolve for given sequence of pieces
                List<ChessPiece> piecesList = ChallengeResolverUtils.createPiecesListForGivenSequence(sequence);
                resolveChallengeForGivenPieceSequence(piecesList);
            }
        } else {
            throw new NoConfigurationException("Challenge configuration not provided");
        }
    }

    public String prettyPrintValidBoards() {
        StringBuilder builder = new StringBuilder();
        builder.append("Number of solutions found: ").append(validChessBoards.size()).append(System.getProperty("line.separator"));
        int i = 1;
        for (ChessBoard chessBoard : validChessBoards.values()) {
            builder.append("----------- Solution nr ").append(i).append(" ----------- ").append(System.getProperty("line.separator"));
            builder.append(chessBoard.prettyPrintChessBoard()).append(System.getProperty("line.separator"));
            i++;
        }

        return builder.toString();
    }

    public List<ChessBoard> getValidChessBoardsList() {
        return new ArrayList<ChessBoard>(validChessBoards.values());
    }

    private void validateChessBoardDimensions(ChallengeConfiguration challengeConfiguration) {
        if (challengeConfiguration.getColumnsGiven() < 1 || challengeConfiguration.getRowsGiven() < 1) {
            throw new InvalidConfigurationException("Invalid board dimensions specified");
        }
    }

    private void resolveChallengeForGivenPieceSequence(List<ChessPiece> piecesList) {
        for (int y = 0; y < configuration.getRowsGiven(); y++) {
            for (int x = 0; x < configuration.getColumnsGiven(); x++) {
                logger.debug("Evaluating board with starting square " + x + "," + y);
                int piecesPlaced = 0;

                ChessBoard chessBoard = new ChessBoard(configuration.getRowsGiven(), configuration.getColumnsGiven());
                ChessBoardSquare startSquare = new ChessBoardSquare(x, y);

                for (int i = 0; i < piecesList.size(); i++) {
                    ChessPiece piece = piecesList.get(i);
                    if (i == 0) {
                        placeFirstPiece(chessBoard, piece, startSquare);
                        piecesPlaced++;
                    } else {
                        boolean isPiecePlacedOnBoard = placePieceOnBoard(chessBoard, piece);
                        if (!isPiecePlacedOnBoard) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Piece not placed.");
                            }
                            //if one piece is not placed, then the whole chessboard is invalid
                            break;
                        } else {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Piece " + piece.getChessPieceSymbol() + " placed");
                            }
                            piecesPlaced++;
                        }
                    }
                }

                //were all pieces placed?
                if (piecesPlaced == piecesList.size()) {
                    validChessBoards.put(chessBoard.generateChessBoardId(), chessBoard);
                    if (logger.isDebugEnabled()) {
                        logger.debug("All pieces placed. Board is valid");
                        logger.debug(System.getProperty("line.separator") + chessBoard.prettyPrintChessBoard());
                    }
                } else {
                    logger.debug("Not all pieces placed. Board is invalid");
                }
            }
        }
    }

    private void placeFirstPiece(ChessBoard chessBoard, ChessPiece firstPiece, ChessBoardSquare startSquare) {
        if (logger.isDebugEnabled()) {
            logger.debug("Placing first piece " + firstPiece.getChessPieceSymbol() + " on square " + startSquare.toStringCoordinates());
        }
        chessBoard.placeFirstChessPiece(firstPiece, startSquare.getColumn(), startSquare.getRow());
    }


    private boolean placePieceOnBoard(ChessBoard chessBoard, ChessPiece pieceToPlace) {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to place " + pieceToPlace.getChessPieceSymbol());
        }
        boolean piecePlaced = chessBoard.placeChessPiece(pieceToPlace);
        if (!piecePlaced && logger.isDebugEnabled()) {
            logger.debug("No place for " + pieceToPlace.getChessPieceSymbol() + " found");
        }
        return piecePlaced;
    }
}
