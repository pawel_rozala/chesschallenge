package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.pieces.ChessPiece;

/**
 * Class representing a single spot on the chess board (can be occupied by a chess piece or in "range" of another piece)
 * <p/>
 * Created by prozala on 2015-06-23.
 */
public class ChessBoardSquare {

    private int column;

    private int row;

    private ChessPiece chessPiece;

    private boolean occupied;

    private boolean threatened;

    public ChessBoardSquare(int column, int row) {
        this.column = column;
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public ChessPiece getChessPiece() {
        return chessPiece;
    }

    public void setChessPiece(ChessPiece chessPiece) {
        this.chessPiece = chessPiece;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setThreatened(boolean threatened) {
        this.threatened = threatened;
    }

    public boolean isThreatened() {
        return threatened;
    }

    public String toStringCoordinates() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append(column);
        sb.append(", ").append(row);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChessBoardSquare{");
        sb.append("column=").append(column);
        sb.append(", row=").append(row);
        sb.append(", chessPiece=").append(chessPiece);
        sb.append(", occupied=").append(occupied);
        sb.append(", threatened=").append(threatened);
        sb.append('}');
        return sb.toString();
    }
}
