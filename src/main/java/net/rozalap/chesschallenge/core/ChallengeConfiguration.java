package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.pieces.ChessPieceEnum;

/**
 * Class holding the Chess Challenge configuration - dimensions of chess board and number of pieces
 *
 * Created by prozala on 2015-06-23.
 */
public class ChallengeConfiguration {

    private int rowsGiven;
    private int columnsGiven;
    private int kingsNumber;
    private int queensNumber;
    private int bishopsNumber;
    private int rooksNumber;
    private int knightsNumber;

    public ChallengeConfiguration(int columnsGiven, int rowsGiven, int kingsNumber, int queensNumber, int bishopsNumber, int rooksNumber, int knightsNumber) {
        this.rowsGiven = rowsGiven;
        this.columnsGiven = columnsGiven;
        this.kingsNumber = kingsNumber;
        this.queensNumber = queensNumber;
        this.bishopsNumber = bishopsNumber;
        this.rooksNumber = rooksNumber;
        this.knightsNumber = knightsNumber;
    }

    public int getRowsGiven() {
        return rowsGiven;
    }

    public void setRowsGiven(int rowsGiven) {
        this.rowsGiven = rowsGiven;
    }

    public int getColumnsGiven() {
        return columnsGiven;
    }

    public void setColumnsGiven(int columnsGiven) {
        this.columnsGiven = columnsGiven;
    }

    public int getKingsNumber() {
        return kingsNumber;
    }

    public void setKingsNumber(int kingsNumber) {
        this.kingsNumber = kingsNumber;
    }

    public int getQueensNumber() {
        return queensNumber;
    }

    public void setQueensNumber(int queensNumber) {
        this.queensNumber = queensNumber;
    }

    public int getBishopsNumber() {
        return bishopsNumber;
    }

    public void setBishopsNumber(int bishopsNumber) {
        this.bishopsNumber = bishopsNumber;
    }

    public int getRooksNumber() {
        return rooksNumber;
    }

    public void setRooksNumber(int rooksNumber) {
        this.rooksNumber = rooksNumber;
    }

    public int getKnightsNumber() {
        return knightsNumber;
    }

    public void setKnightsNumber(int knightsNumber) {
        this.knightsNumber = knightsNumber;
    }

    public String getPieceSequence() {
        StringBuilder pieceSequence = new StringBuilder();
        //create list of pieces
        for (int i = 0; i < kingsNumber; i++) {
            pieceSequence.append(ChessPieceEnum.KING.getSymbol());
        }
        for (int i = 0; i < queensNumber; i++) {
            pieceSequence.append(ChessPieceEnum.QUEEN.getSymbol());
        }
        for (int i = 0; i < bishopsNumber; i++) {
            pieceSequence.append(ChessPieceEnum.BISHOP.getSymbol());
        }
        for (int i = 0; i < rooksNumber; i++) {
            pieceSequence.append(ChessPieceEnum.ROOK.getSymbol());
        }
        for (int i = 0; i < knightsNumber; i++) {
            pieceSequence.append(ChessPieceEnum.KNIGHT.getSymbol());
        }

        return pieceSequence.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChallengeConfiguration{");
        sb.append("rowsGiven=").append(rowsGiven);
        sb.append(", columnsGiven=").append(columnsGiven);
        sb.append(", kingsNumber=").append(kingsNumber);
        sb.append(", queensNumber=").append(queensNumber);
        sb.append(", bishopsNumber=").append(bishopsNumber);
        sb.append(", rooksNumber=").append(rooksNumber);
        sb.append(", knightsNumber=").append(knightsNumber);
        sb.append('}');
        return sb.toString();
    }
}
