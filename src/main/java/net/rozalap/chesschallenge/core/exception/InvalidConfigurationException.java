package net.rozalap.chesschallenge.core.exception;

/**
 * Exception thrown when supplied chess challenge configuration is invalid (board dimension equals zero)
 * <p/>
 * Created by prozala on 2015-06-28.
 */
public class InvalidConfigurationException extends RuntimeException {

    public InvalidConfigurationException() {
    }

    public InvalidConfigurationException(String message) {
        super(message);

    }
}