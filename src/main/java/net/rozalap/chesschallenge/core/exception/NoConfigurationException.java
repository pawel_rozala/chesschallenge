package net.rozalap.chesschallenge.core.exception;

/**
 * Exception thrown when no configuration was supplied to ChallengeResolver
 * <p/>
 * Created by prozala on 2015-06-28.
 */
public class NoConfigurationException extends RuntimeException {

    public NoConfigurationException() {
    }

    public NoConfigurationException(String message) {
        super(message);

    }
}