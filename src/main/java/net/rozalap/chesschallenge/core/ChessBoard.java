package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.pieces.ChessPiece;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Class representing the chess board along with piece placement functionality
 * <p/>
 * Created by prozala on 2015-06-23.
 */
public class ChessBoard {

    final static Logger logger = Logger.getLogger(ChessBoard.class);

    private int maxColumns;

    private int maxRows;

    private ChessBoardSquare[][] chessBoardSquares;

    public int getMaxColumns() {
        return maxColumns;
    }

    public void setMaxColumns(int maxColumns) {
        this.maxColumns = maxColumns;
    }

    public int getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    public ChessBoardSquare[][] getChessBoardSquares() {
        return chessBoardSquares;
    }

    public void setChessBoardSquares(ChessBoardSquare[][] chessBoardSquares) {
        this.chessBoardSquares = chessBoardSquares;
    }

    public ChessBoard(int maxColumns, int maxRows) {
        this.maxColumns = maxColumns;
        this.maxRows = maxRows;

        chessBoardSquares = new ChessBoardSquare[maxColumns][maxRows];

        for (int y = 0; y < maxRows; y++) {
            for (int x = 0; x < maxColumns; x++) {
                ChessBoardSquare square = new ChessBoardSquare(x, y);
                chessBoardSquares[x][y] = square;
            }
        }
    }

    /**
     * Pretty print the chess board and pieces placement
     *
     * @return
     */
    public String prettyPrintChessBoard() {
        StringBuilder stringBuilder = new StringBuilder();
        int previousRow = 0;

        for (int y = 0; y < maxRows; y++) {
            for (int x = 0; x < maxColumns; x++) {
                ChessBoardSquare square = chessBoardSquares[x][y];
                if (previousRow != square.getRow()) {
                    stringBuilder.append(System.getProperty("line.separator"));
                }
                if (square.getChessPiece() != null) {
                    stringBuilder.append(square.getChessPiece().getChessPieceSymbol());
                } else {
                    stringBuilder.append("-");
                }
                previousRow = square.getRow();
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Generate a chessBoard id key for board comparison (to find unique solutions)
     *
     * @return string containing piece placement row by row
     */
    public String generateChessBoardId() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int y = 0; y < maxRows; y++) {
            for (int x = 0; x < maxColumns; x++) {
                ChessBoardSquare square = chessBoardSquares[x][y];

                if (square.getChessPiece() != null) {
                    stringBuilder.append(square.getChessPiece().getChessPieceSymbol());
                } else {
                    stringBuilder.append("-");
                }

            }
        }
        return stringBuilder.toString();

    }

    /**
     * Place first chess piece on given starting square
     *
     * @param chessPiece  - chess piece to be placed
     * @param startingCol - x columnn
     * @param startingRow - y row
     */
    public void placeFirstChessPiece(ChessPiece chessPiece, int startingCol, int startingRow) {
        ChessBoardSquare squareToOccupy = chessBoardSquares[startingCol][startingRow];
        occupySquare(chessPiece, squareToOccupy);
    }

    /**
     * Try to place given chess piece on first free (not occupied and not threatened by other piece) square (columns first)
     *
     * @param chessPiece - chess piece to be placed
     * @return - true if the piece was placed (free square was found), false if there is no free square for this piece
     */
    public boolean placeChessPiece(ChessPiece chessPiece) {
        boolean piecePlaced = false;

        ChessBoardSquare squareToOccupy = findFirstFreeSquare(chessPiece);

        if (squareToOccupy != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found free square: " + squareToOccupy.toStringCoordinates());
            }
            occupySquare(chessPiece, squareToOccupy);
            piecePlaced = true;
        } else {
            logger.debug("No free squares found");
        }

        return piecePlaced;
    }


    private void occupySquare(ChessPiece piece, ChessBoardSquare squareToOccupy) {
        squareToOccupy.setChessPiece(piece);
        squareToOccupy.setOccupied(true);
        markThreatenedSquares(squareToOccupy, piece);
    }

    private ChessBoardSquare findFirstFreeSquare(ChessPiece chessPiece) {
        ChessBoardSquare foundSquare = null;

        for (int y = 0; y < maxRows; y++) {
            for (int x = 0; x < maxColumns; x++) {

                ChessBoardSquare square = chessBoardSquares[x][y];
                if (logger.isTraceEnabled()) {
                    logger.trace("Evaluating square: " + square.toStringCoordinates());
                }
                if (!square.isOccupied() && !square.isThreatened()) {
                    //need to check if placing on this square threatens already placed pieces
                    List<ChessBoardSquare> validMoveSquares = chessPiece.getValidMoveSquares(square, chessBoardSquares);
                    if (!checkValidMoveSquaresForPieces(validMoveSquares)) {
                        foundSquare = square;
                        return foundSquare;
                    }
                }
            }
        }

        return foundSquare;
    }

    private boolean checkValidMoveSquaresForPieces(List<ChessBoardSquare> validMoveSquares) {
        boolean pieceOnValidMoveSquare = false;
        for (ChessBoardSquare square : validMoveSquares) {
            if (square.isOccupied()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Found piece " + square.getChessPiece().getChessPieceSymbol() + " on square " + square.toStringCoordinates());
                }
                pieceOnValidMoveSquare = true;
                break;
            }
        }
        return pieceOnValidMoveSquare;
    }

    private void markThreatenedSquares(ChessBoardSquare occupiedSquare, ChessPiece chessPiece) {
        List<ChessBoardSquare> threatenedSquares = chessPiece.getValidMoveSquares(occupiedSquare, chessBoardSquares);
        for (ChessBoardSquare threatenedSquare : threatenedSquares) {
            threatenedSquare.setThreatened(true);
        }
    }

}
