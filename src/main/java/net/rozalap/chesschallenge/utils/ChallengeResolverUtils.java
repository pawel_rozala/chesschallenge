package net.rozalap.chesschallenge.utils;

import net.rozalap.chesschallenge.pieces.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class with various utility methods for ChallengeResolver
 *
 * Created by prozala on 2015-06-27.
 */
public class ChallengeResolverUtils {

    /**
     * Given the order of piece, retrun a list of chess piece objects for placement on board
     *
     * @param pieceSequence
     * @return
     */
    public static List<ChessPiece> createPiecesListForGivenSequence(String pieceSequence) {
        List<ChessPiece> piecesList = new ArrayList<ChessPiece>();

        for (int i = 0; i < pieceSequence.length(); i++) {
            char pieceSymbol = pieceSequence.charAt(i);

            if (ChessPieceEnum.KING.getSymbol() == pieceSymbol) {
                King king = new King();
                piecesList.add(king);
            } else if (ChessPieceEnum.QUEEN.getSymbol() == pieceSymbol) {
                Queen queen = new Queen();
                piecesList.add(queen);
            } else if (ChessPieceEnum.BISHOP.getSymbol() == pieceSymbol) {
                Bishop bishop = new Bishop();
                piecesList.add(bishop);
            } else if (ChessPieceEnum.ROOK.getSymbol() == pieceSymbol) {
                Rook rook = new Rook();
                piecesList.add(rook);
            } else if (ChessPieceEnum.KNIGHT.getSymbol() == pieceSymbol) {
                Knight knight = new Knight();
                piecesList.add(knight);
            }
        }

        return piecesList;
    }

    /**
     * Given the original order of pieces, generate all possible sequences (without repetition)
     *
     * @param originalPieceSequence
     * @return
     */
    public static Set<String> generatePieceSequencePermutations(String originalPieceSequence) {
        Set<String> result = new HashSet<String>();
        if (originalPieceSequence == null) {
            return null;
        } else if (originalPieceSequence.length() == 0) {
            result.add("");
            return result;
        }

        char firstChar = originalPieceSequence.charAt(0);
        String rem = originalPieceSequence.substring(1);
        Set<String> words = generatePieceSequencePermutations(rem);
        for (String newString : words) {
            for (int i = 0; i <= newString.length(); i++) {
                result.add(addChar(newString, firstChar, i));
            }
        }
        return result;
    }

    private static String addChar(String str, char c, int j) {
        String first = str.substring(0, j);
        String last = str.substring(j);
        return first + c + last;
    }
}
