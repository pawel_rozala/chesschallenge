package net.rozalap.chesschallenge.pieces;

/**
 * Created by prozala on 2015-06-22.
 */
public class Queen extends ChessPiece {

    public Queen() {
    }

    @Override
    public boolean validateMove(int currentXCol, int currentYRow, int newXCol, int newYRow) {
        //Queen's moves like a Rook or a Bishop
        return new Rook().validateMove(currentXCol, currentYRow, newXCol, newYRow) || new Bishop().validateMove(currentXCol, currentYRow, newXCol, newYRow);
    }

    @Override
    public char getChessPieceSymbol() {
        return ChessPieceEnum.QUEEN.getSymbol();
    }
}
