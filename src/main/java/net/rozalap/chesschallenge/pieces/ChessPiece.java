package net.rozalap.chesschallenge.pieces;

import net.rozalap.chesschallenge.core.ChessBoardSquare;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for chess piece
 *
 * Created by prozala on 2015-06-23.
 */
public abstract class ChessPiece {

    public ChessPiece() {
    }

    /**
     * Chess piece should return its corresponding symbol (King = K, etc)
     *
     * @return chess piece symbol
     */
    public abstract char getChessPieceSymbol();

    /**
     * Chess piece should validate moving from one square to another according to its movement rules
     *
     * @return true if move from current square to new square
     */
    protected abstract boolean validateMove(int currentXCol, int currentXRow, int newXCol, int newYRow);

    /**
     * Returns the list of all possible movement squares for given chess piece and square
     *
     * @param startingSquare    - spot which the piece currently occupies
     * @param chessBoardSquares - the array of all the chess board squares
     * @return list of all valid movement squares
     */
    public List<ChessBoardSquare> getValidMoveSquares(ChessBoardSquare startingSquare, ChessBoardSquare[][] chessBoardSquares) {
        List<ChessBoardSquare> validMoveSquares = new ArrayList<ChessBoardSquare>();

        //TODO introduce cache for validMoves - Map<ChessBoardSquare, List<ChessBoardSquare>> validMoveSquaresCache?

        for (ChessBoardSquare[] rows : chessBoardSquares) {
            for (ChessBoardSquare columnSquare : rows) {
                if (validateMove(startingSquare.getColumn(), startingSquare.getRow(), columnSquare.getColumn(), columnSquare.getRow())) {
                    validMoveSquares.add(columnSquare);
                }

            }
        }

        return validMoveSquares;
    }

}
