package net.rozalap.chesschallenge.pieces;

/**
 * Enum of chess pieces used by Chess Challenge with their symbols (pawns are ignored)
 *
 * Created by prozala on 2015-06-24.
 */
public enum ChessPieceEnum {
    KING('K'), QUEEN('Q'), BISHOP('B'), ROOK('R'), KNIGHT('N');

    private char symbol;

    private ChessPieceEnum(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}



