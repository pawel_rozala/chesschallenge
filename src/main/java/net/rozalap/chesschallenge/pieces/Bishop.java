package net.rozalap.chesschallenge.pieces;

/**
 * Created by prozala on 2015-06-22.
 */
public class Bishop extends ChessPiece {

    public Bishop() {
    }

    @Override
    public boolean validateMove(int currentXCol, int currentYRow, int newXCol, int newYRow) {

        if (currentYRow == newYRow || currentXCol == newXCol) {
            //Did not move diagonally
            return false;
        }

        if (Math.abs(newYRow - currentYRow) != Math.abs(newXCol - currentXCol)) {
            return false;
        }

        return true;

    }

    @Override
    public char getChessPieceSymbol() {
        return ChessPieceEnum.BISHOP.getSymbol();
    }
}
