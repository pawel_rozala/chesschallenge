package net.rozalap.chesschallenge.pieces;

/**
 * Created by prozala on 2015-06-22.
 */
public class King extends ChessPiece {

    public King() {
    }

    @Override
    public boolean validateMove(int currentXCol, int currentYRow, int newXCol, int newYRow) {

        if (Math.abs(newYRow - currentYRow) > 1 || Math.abs(newXCol - currentXCol) > 1) {
            if (newXCol - currentXCol == 2 && currentYRow == newYRow) {
                return false;
            } else if (currentXCol - newXCol == 3 && currentYRow == newYRow) {
                return false;
            } else {

                return false;
            }
        }

        return true;
    }

    @Override
    public char getChessPieceSymbol() {
        return ChessPieceEnum.KING.getSymbol();
    }
}
