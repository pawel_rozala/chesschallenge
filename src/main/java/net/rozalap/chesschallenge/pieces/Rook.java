package net.rozalap.chesschallenge.pieces;

/**
 * Created by prozala on 2015-06-22.
 */
public class Rook extends ChessPiece {

    public Rook() {
    }

    @Override
    public boolean validateMove(int currentXCol, int currentYRow, int newXCol, int newYRow) {
        //The rook can move along the same column or row (horizontally or vertically)
        if (newXCol == currentXCol || newYRow == currentYRow) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public char getChessPieceSymbol() {
        return ChessPieceEnum.ROOK.getSymbol();
    }
}
