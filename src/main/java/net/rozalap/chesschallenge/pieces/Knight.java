package net.rozalap.chesschallenge.pieces;

/**
 * Created by prozala on 2015-06-22.
 */
public class Knight extends ChessPiece {

    public Knight() {
    }

    @Override
    public boolean validateMove(int currentXCol, int currentYRow, int newXCol, int newYRow) {
        if (Math.abs(newXCol - currentXCol) == 2 && Math.abs(newYRow - currentYRow) == 1) {
            return true;
        }

        if (Math.abs(newXCol - currentXCol) == 1 && Math.abs(newYRow - currentYRow) == 2) {
            return true;
        }

        return false;
    }

    @Override
    public char getChessPieceSymbol() {
        return ChessPieceEnum.KNIGHT.getSymbol();
    }
}
