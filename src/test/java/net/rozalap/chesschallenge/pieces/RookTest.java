package net.rozalap.chesschallenge.pieces;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RookTest {

    @Test
    public void testValidateMove() throws Exception {

        Rook testRook = new Rook();
        assertEquals(true, testRook.validateMove(0, 0, 1, 0));
        assertEquals(true, testRook.validateMove(0, 0, 0, 1));
        assertEquals(false, testRook.validateMove(0, 0, 1, 1));
        assertEquals(true, testRook.validateMove(0, 0, 0, 2));

        assertEquals(true, testRook.validateMove(1, 0, 0, 0));
        assertEquals(true, testRook.validateMove(1, 0, 3, 0));
        assertEquals(false, testRook.validateMove(1, 0, 0, 2));
        assertEquals(false, testRook.validateMove(1, 0, 2, 1));

        assertEquals(false, testRook.validateMove(1, 1, 3, 0));

    }

    @Test
    public void testGetChessPieceSymbol() throws Exception {
        Rook testRook = new Rook();
        assertEquals('R', testRook.getChessPieceSymbol());
    }
}