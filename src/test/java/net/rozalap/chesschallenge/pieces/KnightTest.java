package net.rozalap.chesschallenge.pieces;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KnightTest {

    @Test
    public void testValidateMove() throws Exception {

        Knight testKnight = new Knight();

        assertEquals(true, testKnight.validateMove(0, 0, 2, 1));
        assertEquals(true, testKnight.validateMove(0, 0, 1, 2));
        assertEquals(false, testKnight.validateMove(0, 0, 2, 2));

        assertEquals(true, testKnight.validateMove(1, 1, 0, 3));
        assertEquals(true, testKnight.validateMove(1, 1, 2, 3));
    }

    @Test
    public void testGetChessPieceSymbol() throws Exception {
        Knight testKnight = new Knight();
        assertEquals('N', testKnight.getChessPieceSymbol());
    }
}