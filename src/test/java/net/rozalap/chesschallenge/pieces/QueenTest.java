package net.rozalap.chesschallenge.pieces;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QueenTest {

    @Test
    public void testValidateMove() throws Exception {

        Queen testQueen = new Queen();

        //row below
        assertEquals(true, testQueen.validateMove(1, 1, 0, 0));
        assertEquals(true, testQueen.validateMove(1, 1, 1, 0));
        assertEquals(true, testQueen.validateMove(1, 1, 2, 0));

        //same row
        assertEquals(true, testQueen.validateMove(1, 1, 0, 1));
        assertEquals(true, testQueen.validateMove(1, 1, 2, 1));

        //row above
        assertEquals(true, testQueen.validateMove(1, 1, 0, 2));
        assertEquals(true, testQueen.validateMove(1, 1, 1, 2));
        assertEquals(true, testQueen.validateMove(1, 1, 2, 2));

        assertEquals(true, testQueen.validateMove(1, 1, 1, 3));
        assertEquals(false, testQueen.validateMove(1, 1, 2, 3));
        assertEquals(true, testQueen.validateMove(1, 1, 3, 3));
        assertEquals(false, testQueen.validateMove(1, 1, 3, 5));
        assertEquals(true, testQueen.validateMove(2, 2, 0, 0));


    }

    @Test
    public void testGetChessPieceSymbol() throws Exception {
        Queen testQueen = new Queen();
        assertEquals('Q', testQueen.getChessPieceSymbol());
    }
}