package net.rozalap.chesschallenge.pieces;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KingTest {

    @Test
    public void testValidateMove() throws Exception {

        King testKing = new King();

        //row below
        assertEquals(true, testKing.validateMove(1, 1, 0, 0));
        assertEquals(true, testKing.validateMove(1, 1, 1, 0));
        assertEquals(true, testKing.validateMove(1, 1, 2, 0));

        //same row
        assertEquals(true, testKing.validateMove(1, 1, 0, 1));
        assertEquals(true, testKing.validateMove(1, 1, 2, 1));

        //row above
        assertEquals(true, testKing.validateMove(1, 1, 0, 2));
        assertEquals(true, testKing.validateMove(1, 1, 1, 2));
        assertEquals(true, testKing.validateMove(1, 1, 2, 2));

        assertEquals(false, testKing.validateMove(1, 1, 1, 3));
        assertEquals(false, testKing.validateMove(1, 1, 2, 3));
        assertEquals(false, testKing.validateMove(1, 1, 3, 3));
        assertEquals(false, testKing.validateMove(1, 1, 3, 5));
        assertEquals(false, testKing.validateMove(2, 2, 0, 0));

    }

    @Test
    public void testGetChessPieceSymbol() throws Exception {
        King testKing = new King();
        assertEquals('K', testKing.getChessPieceSymbol());
    }
}