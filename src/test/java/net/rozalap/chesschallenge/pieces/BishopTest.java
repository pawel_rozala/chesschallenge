package net.rozalap.chesschallenge.pieces;

import org.junit.Test;

import static org.junit.Assert.*;

public class BishopTest {

    @Test
    public void testValidateMove() throws Exception {

        Bishop testBishop = new Bishop();
        assertEquals(true, testBishop.validateMove(0, 0, 1, 1));
        assertEquals(true, testBishop.validateMove(0, 0, 2, 2));
        assertEquals(false, testBishop.validateMove(0, 0, 1, 0));

        assertEquals(true, testBishop.validateMove(1, 0, 2, 1));
        assertEquals(true, testBishop.validateMove(1, 0, 3, 2));
        assertEquals(true, testBishop.validateMove(1, 0, 0, 1));
        assertEquals(false, testBishop.validateMove(1, 0, 1, 1));

        assertEquals(false, testBishop.validateMove(0, 0, 1, 2));

    }

    @Test
    public void testGetChessPieceSymbol() throws Exception {
        Bishop testBishop = new Bishop();
        assertEquals('B', testBishop.getChessPieceSymbol());
    }
}