package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.ChallengeResolver;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChallengeResolverImplTest {

    @Test
    public void testResolveChallenge() throws Exception {

        //Assert that our resolver finds the same number of configurations as in ChessChallenge.pdf

        //Input: 3×3 board containing 2 Kings and 1 Rook
        //Output: 4 solutions
        ChallengeConfiguration challengeConfiguration = new ChallengeConfiguration(3, 3, 2, 0, 0, 1, 0);
        ChallengeResolver resolver = new ChallengeResolverImpl();
        resolver.setChallengeConfiguration(challengeConfiguration);
        resolver.resolveChallenge();
        assertEquals(4, resolver.getValidChessBoardsList().size());

        //Input: 4×4 board containing 2 Rooks and 4 Knights.
        //Output: 8 solutions
        challengeConfiguration = new ChallengeConfiguration(4, 4, 0, 0, 0, 2, 4);
        resolver = new ChallengeResolverImpl();
        resolver.setChallengeConfiguration(challengeConfiguration);
        resolver.resolveChallenge();
        assertEquals(8, resolver.getValidChessBoardsList().size());

    }
}