package net.rozalap.chesschallenge.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChallengeConfigurationTest {

    @Test
    public void testGetPiecesSequence() throws Exception {
        ChallengeConfiguration configurationTest = new ChallengeConfiguration(1, 1, 1, 1, 1, 1, 1);
        assertEquals("KQBRN", configurationTest.getPieceSequence());

        configurationTest = new ChallengeConfiguration(1, 1, 2, 1, 2, 0, 0);
        assertEquals("KKQBB", configurationTest.getPieceSequence());

        configurationTest = new ChallengeConfiguration(1, 1, 0, 0, 3, 2, 1);
        assertEquals("BBBRRN", configurationTest.getPieceSequence());
    }

}