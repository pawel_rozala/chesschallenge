package net.rozalap.chesschallenge.core;

import net.rozalap.chesschallenge.pieces.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChessBoardTest {

    @Test
    public void testGenerateChessBoardId() throws Exception {
        ChessBoard testChessBoard = new ChessBoard(2, 2);
        ChessBoardSquare[][] squares = testChessBoard.getChessBoardSquares();

        squares[0][0].setChessPiece(new King());
        squares[1][1].setChessPiece(new King());

        assertEquals("K--K", testChessBoard.generateChessBoardId());

        testChessBoard = new ChessBoard(3, 3);
        squares = testChessBoard.getChessBoardSquares();

        squares[1][0].setChessPiece(new King());

        squares[0][1].setChessPiece(new Queen());
        squares[1][1].setChessPiece(new King());

        squares[0][2].setChessPiece(new Bishop());
        squares[1][2].setChessPiece(new Rook());
        squares[2][2].setChessPiece(new Knight());

        assertEquals("-K-QK-BRN", testChessBoard.generateChessBoardId());
    }

    @Test
    public void testPlaceFirstChessPiece() throws Exception {
        ChessBoard testChessBoard = new ChessBoard(3, 3);
        testChessBoard.placeFirstChessPiece(new Bishop(), 1, 1);

        assertEquals(true, testChessBoard.getChessBoardSquares()[1][1].isOccupied());

        testChessBoard = new ChessBoard(4, 4);
        testChessBoard.placeFirstChessPiece(new Bishop(), 2, 3);

        assertEquals(true, testChessBoard.getChessBoardSquares()[2][3].isOccupied());
    }

    @Test
    public void testPlaceChessPiece() throws Exception {
        ChessBoard testChessBoard = new ChessBoard(3, 3);
        testChessBoard.placeFirstChessPiece(new King(), 0, 0);
        testChessBoard.placeChessPiece(new King());
        assertEquals(true, testChessBoard.getChessBoardSquares()[2][0].isOccupied());

        testChessBoard = new ChessBoard(3, 3);
        testChessBoard.placeFirstChessPiece(new King(), 2, 2);
        testChessBoard.placeChessPiece(new King());
        assertEquals(true, testChessBoard.getChessBoardSquares()[0][0].isOccupied());
    }
}